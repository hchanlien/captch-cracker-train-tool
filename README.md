# 验证码识别训练工具
说明：本程序在于学习JAVA验证码别及交流，不可用于网络攻击和强力验证码爆破。
### 说明
- 贯穿验证码下载、图像处理、字符检测分割、对象识别、脚本处理等技术
- 以学习交流为目的，代码注释及文档也在逐步完善
- java语言验证码学习项目，拥有完整的训练、检测、识别过程的开源项目 在整个开源社区来说都相对较少
- 本人目前也正在学习图片识别相关技术，代码及文档持续更新中

### 包含功能
- 图像过滤器，包括大部份的图像处理，  主要过滤器使用jhlabs类库对图像做前期处理
  - 象素化：彩色网格、晶格化、象素风格化、马赛克
  - 噪点处理：去斑、去除噪点1、去除噪点2、去除孤立点、去除干扰线
  - 形态处理：去除白边、旋转图像、图像缩放、裁剪图像、透视变形
  - 效果处理：发光、取消锐化、锐化、浮雕、镶边
  - 模糊处理：智能模糊、盒状模糊、简单模糊、高斯模糊
  - 边缘检测：canny、拉普拉斯、高斯差值、边缘检测、轮廓
  - 颜色处理：三阶色调整、中值降噪、二值化、伽码、对比度调整、曝光、曝光过度、混合RGB通道、清除背景、灰度处理、腐蚀、色彩增值、色彩分离、过滤颜色、颜色反转
- 图像分割器，包括固定分割法、投影分割法、联通分割法、水滴分割法
- 生成特征码，将处理后的图像生成16*16方阵的特征码，用于识别对比使用
- 图像识别 使用训练后的图像特征库对比进行图像识别
### <font color ="#ff3333">即将开放功能</font>
- <font color ="#ff3333">动图验证码识别（已完成）</font>
- <font color ="#ff3333">图像直方图展示（已完成）</font>
- <font color ="#ff3333">自定义脚本过滤器 (已完成)</font>
- <font color ="#ff3333">截图获取验证码(已完成)</font>
- <font color ="#ff3333">验证码识别服务端，配合浏览器插件实现自动识别验证码（已完成）</font>
- <font color ="#ff9d00">RGB、HSL过滤器 （进行中）</font>
- <font color ="#ff9d00">通道过滤器 （进行中）</font>
- <font color ="#444444">行为验证码识别、图形点击验证码识别 （未完成）</font>
### 脚本的配置
#### 下载脚本配置  
url 关键字=输入的图片地址
1. 获取验证码接口 URL能直接返回图像的直接使用默认的脚本
```javascript
javax.imageio.ImageIO.read(cn.hutool.http.HttpUtil.createGet(url).execute().bodyStream());
```
也可以写成
```javascript
cn.hutool.http.HttpRequest request=cn.hutool.http.HttpUtil.createGet(url);
cn.hutool.http.HttpResponse response=request.execute();
javax.imageio.ImageIO.read(response.bodyStream());
```
2. 获取验证码接口 URL访问时返回JSON字符串中图像为BASE64的

```javascript
com.alibaba.fastjson.JSONObject json=com.alibaba.fastjson.JSON.parseObject(cn.hutool.http.HttpUtil.createGet(url).execute().body());
json=json.getJSONObject("data");
ImageUtils.base64ToImg(json.getString("imageBase64Data"));
```
3. 获取验证码接口 URL请求前需要设置额外参数的
```javascript
cn.hutool.http.HttpRequest  request=cn.hutool.http.HttpUtil.createPost(url);//为post请求
request.header("token","xxxxx");  //请求增加报文头
request.form("id","xxxxx");  //请求增加参数
cn.hutool.http.HttpResponse  response=request.execute();
javax.imageio.ImageIO.read(response.bodyStream());
```

4. 获取验证码接口 URL请求用做重定向跳转的
```javascript
cn.hutool.http.HttpRequest  request=cn.hutool.http.HttpUtil.createGet(url);
cn.hutool.http.HttpResponse  response=request.execute();
String tmp=response.header("location");   //获取重定向地址
javax.imageio.ImageIO.read(cn.hutool.http.HttpUtil.createGet(tmp).execute().bodyStream());
```
#### 识别脚本配置 灵活配置增加识别正确率
result 关键字为识别结果
1. 默认直接配置
```javascript
result
```
2. 如验证码为表达式 识别结果需要计算
```javascript
CalculationUtils.getInt(result);  //此处为表达式计算并返回结果
```

3. 有些验证码粘连不适合投影分割时，可使用联通分割法配合脚本增加识别率，如图1
```javascript
if(result.charAt(1)>=48&&result.charAt(1)<=57){
    CalculationUtils.getInt(""+result.substring(0,1)+"*"+result.substring(1,2))
}else{
    result=result.replaceAll("//","T");
    result=result.replaceAll("/-","T");
    result=result.replaceAll("-/","T");
    result=result.replaceAll("--","T");
    result=result.replaceAll("T-","T");
    result=result.replaceAll("T/","T");
    result=result.replaceAll("-T","T");
    result=result.replaceAll("/T","T");
    result=result.replaceAll("TT","T");
    result=result.replaceAll("T","*");
    CalculationUtils.getInt(result.substring(0,3))
}
```

### 功能预览
![界面](resources/p1.jpg "界面")
![界面](resources/p2.jpg "界面")
![界面](resources/p3.jpg "界面")
![界面](resources/p4.jpg "界面")
![界面](resources/p5.jpg "界面")

验证码识别类库源码请转到: [captcha-cracker 验证码识别类库](https://gitee.com/guofarui/captcha-cracker)