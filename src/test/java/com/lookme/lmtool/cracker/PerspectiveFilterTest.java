package com.lookme.lmtool.cracker;

import com.jhlabs.image.PerspectiveFilter;
import sun.awt.image.BufferedImageGraphicsConfig;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class PerspectiveFilterTest {

    public static void main(String[] args) throws IOException {
        BufferedImage srcImg= ImageIO.read(new File("C:\\Users\\Administrator\\Desktop\\1.jpg"));
        PerspectiveFilter filter=new PerspectiveFilter();
        filter.setCorners(110,110,-11,11,-11,-91,4,-10);
        BufferedImage dstImg =  newBufferedImage(srcImg.getWidth(),srcImg.getHeight());
        BufferedImage dst=filter.filter(srcImg, dstImg);
        System.out.print(dst);
    }

    public static BufferedImage newBufferedImage(int imgWidth, int imgHeight) {
        BufferedImage bufImg = new BufferedImage(imgWidth, imgHeight, BufferedImage.TYPE_4BYTE_ABGR);
        bufImg = BufferedImageGraphicsConfig.getConfig(bufImg).createCompatibleImage(imgWidth, imgHeight, Transparency.TRANSLUCENT);
        return bufImg;
    }
}
