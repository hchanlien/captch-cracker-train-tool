package com.lookme.lmtool.view;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import com.jfoenix.controls.JFXButton;
import javafx.fxml.FXML;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.TabPane;


public abstract  class RootLayoutView implements Initializable {

    @FXML
    protected Menu menuProject;

    @FXML
    protected MenuItem menuNewProject;

    @FXML
    protected MenuItem menuOpenProject;

    @FXML
    protected MenuItem menuSaveProject;

    @FXML
    protected MenuItem menuExportProjectLib;

    @FXML
    protected MenuItem menuExit;

    @FXML
    protected Menu menuItem;

    @FXML
    protected MenuItem menuAddItem;

    @FXML
    protected MenuItem menuRenameItem;

    @FXML
    protected MenuItem menuDelItem;

    @FXML
    protected MenuItem mennuExportItemLib;

    @FXML
    protected JFXButton btnNewItem;

    @FXML
    protected JFXButton btnDelItem;

    @FXML
    protected TabPane tabPane;


    @FXML
    protected SeparatorMenuItem menuHistorySeparator;
}
