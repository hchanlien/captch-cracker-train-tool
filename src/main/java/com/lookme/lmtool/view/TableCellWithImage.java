package com.lookme.lmtool.view;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.TableCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.awt.image.BufferedImage;


public class TableCellWithImage<T> extends TableCell<T, BufferedImage> {
    private final ImageView image;
    int maxHeight=200;
//    BooleanProperty is_image_visible_ = new SimpleBooleanProperty( false );

    public TableCellWithImage(int maxHeight) {
        this.maxHeight=maxHeight;
        // add ImageView as graphic to display it in addition
        // to the text in the cell
        image = new ImageView(new Image( getClass().getResourceAsStream("/images/ocr.png")));
        image.setFitWidth(48);
        image.setFitHeight(48);
        image.setPreserveRatio(true);

        setGraphic(image);
//        setMinHeight(70);
        setGraphicTextGap(10);
        setContentDisplay(ContentDisplay.RIGHT);
//
//        setOnMouseEntered(mouseEvent -> {
//            is_image_visible_.set(true);
//        });
//
//        setOnMouseExited(mouseEvent -> {
//            is_image_visible_.set(false);
//        });

//        image.visibleProperty().bind(is_image_visible_);
    }

    @Override
    protected void updateItem(BufferedImage item, boolean empty) {
        super.updateItem(item, empty);
        if (empty || item == null) {
            // set back to look of empty cell
            setText(null);
            setGraphic(null);
        } else {
            if(item.getHeight()>=maxHeight){
                int width=item.getHeight()/maxHeight*item.getWidth();
                image.setFitWidth(width);
                image.setFitHeight(maxHeight);
            }else {
                image.setFitWidth(item.getWidth());
                image.setFitHeight(item.getHeight());
            }
            image.setImage(SwingFXUtils.toFXImage(item, null));
//            setText(item);
            setGraphic(image);
        }
    }
}