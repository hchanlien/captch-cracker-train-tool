package com.lookme.lmtool.view;
import com.jfoenix.controls.JFXCheckBox;
import com.lookme.lmtool.cracker.filter.IFilter;
import com.lookme.lmtool.cracker.spliter.ISpliter;
import com.lookme.lmtool.model.CharAspect;
import javafx.fxml.Initializable;

import com.jfoenix.controls.JFXButton;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebView;

import java.awt.image.BufferedImage;

public abstract class CaptchaCrackerView  implements Initializable {

    @FXML
    protected TextField txtFilePath;

    @FXML
    protected JFXButton btnBrowser;

    @FXML
    protected JFXButton btnLoad;

    @FXML
    protected JFXButton btnReOcr;

    @FXML
    protected JFXButton btnExportFeatureLib;
    @FXML
    protected JFXButton btnImportFeatureLib;

    @FXML
    protected ImageView imgSource;

    @FXML
    protected TableView<IFilter> tableFilter;

    @FXML
    protected TableColumn<IFilter, Boolean> colFilterSelect;

    @FXML
    protected TableColumn<IFilter, String> colFilterName;

    @FXML
    protected TableColumn<IFilter, String> colFilterParam;

    @FXML
    protected TableColumn<IFilter, BufferedImage> colFilterPreviewImage;

    @FXML
    protected JFXButton btnAddFilter;

    @FXML
    protected JFXButton btnDelFilter;

    @FXML
    protected JFXButton btnUpFilter;

    @FXML
    protected JFXButton btnDownFilter;

    @FXML
    protected ImageView imgPreview;

    @FXML
    protected HBox paneResultContainer;

    @FXML
    protected TableView<CharAspect> tableFeatureLib;

    @FXML
    protected TableColumn<CharAspect, String> colCharIndex;

    @FXML
    protected TableColumn<CharAspect, String> colCharName;

    @FXML
    protected TableColumn<CharAspect, Integer> colCharSize;

    @FXML
    protected TableColumn<CharAspect, BufferedImage> colCharImage;

    @FXML
    protected TableColumn<CharAspect, String> colCharFeature;

    @FXML
    protected TableColumn<CharAspect, String> colOpt;
    @FXML
    protected FlowPane paneFilterParam;
    @FXML
    protected FlowPane paneSpliterParam;
    @FXML
    protected VBox paneLeft;
    @FXML
    protected VBox panePreview;
    @FXML
    protected HBox paneSpliter;
    @FXML
    protected TitledPane paneFilter;
    @FXML
    protected TitledPane paneFilterParamContainer;
    @FXML
    protected TitledPane paneSourceImg;
    @FXML
    protected Label lblImageInfo;
    @FXML
    protected Label lblOcrResult;
    @FXML
    protected Label lblFilterImgInfo;
    @FXML
    protected Label lblPreviewInfo;
    @FXML
    protected Label lblFilterCostTime;
    @FXML
    protected Label lblOcrCostTime;
    @FXML
    protected ChoiceBox<ISpliter> cbSpliter;

    @FXML
    protected WebView webViewDownScript;



    @FXML
    protected JFXCheckBox chkShowGrid;

    @FXML
    protected JFXCheckBox chkShowSplitLine;

    @FXML
    protected JFXCheckBox chkShowOcrResult;



    @FXML
    protected TitledPane paneOcrResult;
    @FXML
    protected JFXButton btnApplyDownScriptConfig;

    @FXML
    protected JFXButton btnResetDownScriptConfig;

    @FXML
    protected WebView webViewOcrScript;

    @FXML
    protected JFXButton btnApplyOcrScriptConfig;

    @FXML
    protected JFXButton btnResetOcrScriptConfig;
    @FXML
    protected JFXButton btnClearFeatureLib;
    @FXML
    protected Tab tab1;
    @FXML
    protected Tab tab2;
    @FXML
    protected Tab tab3;
    @FXML
    protected Tab tab4;

}
