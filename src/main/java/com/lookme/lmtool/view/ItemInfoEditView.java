package com.lookme.lmtool.view;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;

public abstract class ItemInfoEditView  implements Initializable {
    @FXML
    protected TextField txtItemId;

    @FXML
    protected TextField txtItemName;

}
